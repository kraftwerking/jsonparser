package com.kraftwerking.hackerrank;

/**
 * Created by rmilitante on 2/14/17.
 */

import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import com.google.gson.Gson;


public class JsonParser {

    public static void main(String[] args) {
        JsonParser obj = new JsonParser();
        String filename = "BKE00683.json";
        obj.getFile(filename);
        System.out.println("done");
    }


    private void getFile(String fileName) {

        //Get file from resources folder
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        Gson gson = new Gson();
        int totalReadings = 0;
        int totalSuccess = 0;
        int totalFailure = 0;

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                //System.out.println(line);
                AuditObject obj = gson.fromJson(line, AuditObject.class);
                //System.out.println(obj.readingsListSize);
                //System.out.println(obj.readingSuccessSize);
                //System.out.println(obj.readingFailureSize);

                totalReadings = totalReadings + obj.readingsListSize;
                totalSuccess = totalSuccess + obj.readingSuccessSize;
                totalFailure = totalFailure + obj.readingFailureSize;

            }

            scanner.close();

            System.out.println(fileName);
            System.out.println("totalReadings " + totalReadings);
            System.out.println("totalSuccess " + totalSuccess);
            System.out.println("totalFailure " + totalFailure);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    protected class AuditObject{

        String key;
        String assetId;
        String component;
        String uuid;
        String id;
        String timestamp0;
        String timestamp1;
        int readingsListSize;
        int readingSuccessSize;
        int readingFailureSize;

    }

}